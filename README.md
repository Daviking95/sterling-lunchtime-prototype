# sterling-lunchtime
A flutter application for ordering lunch at Sterling Bank HQ

## App Distribution Link
- https://appdistribution.firebase.dev/i/5a44abc370fccd46

## App Screenshots
| Screenshots  | Screenshots  | Screenshots |
| :------------ |:---------------:| -----:|
| ![account-screen](/uploads/128667209e4dd2620c2c6c6bc99e81cd/account-screen.png)      | ![dashboard-screen](/uploads/8697b496bca388a25ae24cf8be404314/dashboard-screen.png) | ![launch-screen](/uploads/8574354c485961d44822678e4ea0407a/launch-screen.png) |
| ![menu-screen](/uploads/5361f0fa136393454c6ff2041c6d4d4f/menu-screen.png)      | ![order-history-screen](/uploads/aa33210a4b4e3496bf3413cbb8e73c4e/order-history-screen.png)        |   ![order-screen](/uploads/b24c5f4a946478b60404adb5fec419e1/order-screen.png) |
| ![start-screen](/uploads/9bc044feae2e5501f9634fe07dbd4886/start-screen.png) |         |     |

